FROM golang:1.19-alpine

COPY ./ /go/src/nats-sub
WORKDIR /go/src/nats-sub

RUN apk update &&\
    apk add --no-cache postgresql14-client

RUN chmod +x wait-for-postgres.sh

RUN apk update &&\
    apk add --no-cache make &&\
    make build app=nats-pub &&\
    make build app=nats-sub