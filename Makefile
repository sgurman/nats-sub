APP_NAME := $(app)

.DEFAULT_GOAL := run

.PHONY: build
build:
	go mod vendor
	go build -mod=vendor -o ./bin/${APP_NAME} ./cmd/${APP_NAME}

.PHONY: run
run: build
	./bin/${APP_NAME}
	rm -rf ./bin

.PHONY: test
test:
	go test -v -coverpkg=./... -coverprofile=cover.out ./...
	go tool cover -html=cover.out -o cover.html

.PHONY: lint
lint:
	golangci-lint -c golangci.yml run -v ./...

.PHONY: compose_up
compose_up:
	docker compose up -d --build

.PHONY: compose_stop
compose_stop:
	docker compose stop

.PHONY: compose_rm
compose_rm:
	docker compose rm -f