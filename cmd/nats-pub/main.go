package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/nats-io/stan.go"
)

const (
	clusterID = "stan-cluster"
	clientID  = "pub-gurman"
	subject   = "orders"
)

func main() {
	log.Printf("connecting `%s` to nats streaming server with cluster_id `%s`",
		clientID, clusterID,
	)
	sc, err := stan.Connect(clusterID, clientID)
	if err != nil {
		log.Fatalf("stan Connect err: %s", err)
	}

	ctx, cancel := signal.NotifyContext(
		context.Background(),
		syscall.SIGINT, syscall.SIGTERM,
	)
	go func() {
		fmt.Println(`
Enter Order UID to generate and publish random Order,
or "invalid" to publish invalid data, or "quit" to exit app`)

		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			str := scanner.Text()
			if str == "quit" {
				cancel()
				return
			}
			if str == "invalid" {
				if err = sc.Publish(subject, []byte(str)); err != nil {
					log.Printf("stan Publish err: %s", err)
					cancel()
				}
				continue
			}

			var order entity.Order
			if err = gofakeit.Struct(&order); err != nil {
				log.Printf("gofakeit Struct generate err: %s", err)
				cancel()
			}
			order.UID = str
			order.Payment.Transaction = str
			order.Delivery.Zip = gofakeit.Zip()
			order.Delivery.Email = gofakeit.Email()

			if err = sc.Publish(subject, order.GetJSON()); err != nil {
				log.Printf("stan Publish err: %s", err)
				cancel()
			}
		}
	}()

	<-ctx.Done()
	log.Printf("app shutdowned gracefully: exiting ...")

	if err = sc.Close(); err != nil {
		log.Printf("stan Close err: %s", err)
	}
}
