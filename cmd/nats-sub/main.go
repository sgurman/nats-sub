package main

import (
	"flag"
	"log"

	"gitlab.com/sgurman/nats-sub/config"
	"gitlab.com/sgurman/nats-sub/internal/app"
)

var configPath = flag.String("config", "./config/config.yml", "App config path")

func main() {
	flag.Parse()

	cfg, err := config.New(*configPath)
	if err != nil {
		log.Panicf("main - config New err: %s", err)
	}

	app.Run(cfg)
}
