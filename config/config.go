package config

import (
	"fmt"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	HTTPServer HTTPServerConfig    `yaml:"http_server"`
	Nats       NatsStreamingConfig `yaml:"nats_streaming"`
	PG         PGConfig
}

type HTTPServerConfig struct {
	Port      string        `yaml:"port" env-required:"true"`
	RWTimeout time.Duration `yaml:"timeout" env-default:"3s"`
}

type NatsStreamingConfig struct {
	URL       string `yaml:"url" env-required:"true"`
	ClusterID string `yaml:"cluster_id" env-required:"true"`
	ClientID  string `yaml:"client_id" env-required:"true"`
}

type PGConfig struct {
	Address  string `env:"DB_ADDR" env-required:"true"`
	DBName   string `env:"DB_NAME" env-required:"true"`
	Username string `env:"DB_USER" env-required:"true"`
	Password string `env:"DB_PASSW" env-required:"true"`
}

func New(configPath string) (Config, error) {
	var cfg Config
	if err := cleanenv.ReadConfig(configPath, &cfg); err != nil {
		return Config{}, fmt.Errorf("cleanenv ReadConfig err: %w", err)
	}
	if err := cleanenv.ReadEnv(&cfg.PG); err != nil {
		return Config{}, fmt.Errorf("cleanenv ReadEnv err: %w", err)
	}
	return cfg, nil
}
