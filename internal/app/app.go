package app

import (
	"context"
	"errors"
	"net/http"
	"os/signal"
	"syscall"

	"gitlab.com/sgurman/nats-sub/config"
	httpapi "gitlab.com/sgurman/nats-sub/internal/handler/http"
	stansub "gitlab.com/sgurman/nats-sub/internal/handler/stan"
	"gitlab.com/sgurman/nats-sub/internal/repository/localmemory"
	"gitlab.com/sgurman/nats-sub/internal/repository/postgresql"
	orderservice "gitlab.com/sgurman/nats-sub/internal/service/order"
	"gitlab.com/sgurman/nats-sub/pkg/http/middleware"
	httpserver "gitlab.com/sgurman/nats-sub/pkg/http/server"
	"gitlab.com/sgurman/nats-sub/pkg/logger"
	"gitlab.com/sgurman/nats-sub/pkg/postgres"

	"github.com/nats-io/stan.go"
)

func Run(cfg config.Config) {
	l := logger.New()
	defer l.Sync() // nolint:errcheck

	l.Infof("connecting `%s` to nats streaming server with cluster_id `%s`",
		cfg.Nats.ClientID, cfg.Nats.ClusterID,
	)
	sc, err := stan.Connect(cfg.Nats.ClusterID, cfg.Nats.ClientID, stan.NatsURL(cfg.Nats.URL))
	if err != nil {
		l.Panicf("app Run - stan Connect err: %s", err)
	}
	defer sc.Close() // nolint:errcheck

	l.Infof("connecting to postresql at `%s`", cfg.PG.Address)
	pg, err := postgres.New(cfg.PG.Address, cfg.PG.DBName, cfg.PG.Username, cfg.PG.Password)
	if err != nil {
		l.Panicf("app Run - postgres New err: %s", err)
	}
	defer pg.Close() // nolint:errcheck

	l.Info("orders cache initialization")
	ordersvc, err := orderservice.New(
		postgresql.NewOrderRepository(pg),
		localmemory.NewOrderCache(),
	)
	if err != nil {
		l.Panicf("app Run - orderservice New err: %s", err)
	}

	if err = stansub.New(sc, ordersvc, l); err != nil {
		l.Panicf("app Run - stansub New err: %s", err)
	}

	router, err := httpapi.NewRouter(ordersvc, l)
	if err != nil {
		l.Panicf("app Run - httpapi NewRouter err: %s", err)
	}

	router = middleware.AccessLog(router, l)
	router = middleware.PanicRecovery(router, l)
	httpsrv := httpserver.New(cfg.HTTPServer.Port, cfg.HTTPServer.RWTimeout, router)

	ctx, cancel := signal.NotifyContext(
		context.Background(),
		syscall.SIGINT, syscall.SIGTERM,
	)
	go func() {
		l.Infof("starting http server at `:%s`", cfg.HTTPServer.Port)
		if err = httpsrv.Start(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			l.Errorf("app Run - server Start err: %s", err)
		}
		cancel()
	}()

	<-ctx.Done()
	l.Info("app shutdowned gracefully: exiting ...")

	if err = httpsrv.Stop(); err != nil {
		l.Errorf("app Run - server Stop err: %s", err)
	}
}
