package entity

type Item struct {
	ChrtID      uint64 `json:"chrt_id" validate:"required"`
	TrackNumber string `json:"track_number" validate:"required"`
	Price       uint   `json:"price" validate:"required"`
	RID         string `json:"rid" validate:"required"`
	Name        string `json:"name" validate:"required"`
	Sale        int    `json:"sale"`
	Size        string `json:"size" validate:"required"`
	TotalPrice  uint   `json:"total_price" validate:"required"`
	NmID        uint64 `json:"nm_id" validate:"required"`
	Brand       string `json:"brand" validate:"required"`
	Status      int    `json:"status"`
}
