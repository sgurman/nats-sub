package entity

import (
	"encoding/json"
	"errors"
	"time"

	"github.com/go-playground/validator/v10"
)

type Order struct {
	UID          string    `json:"order_uid" validate:"eqcsfield=Payment.Transaction"`
	TrackNumber  string    `json:"track_number" validate:"required"`
	Entry        string    `json:"entry" validate:"required"`
	Delivery     Delivery  `json:"delivery" validate:"required"`
	Payment      Payment   `json:"payment" validate:"required"`
	Items        []Item    `json:"items" validate:"required"`
	Locale       string    `json:"locale" validate:"required"`
	InternalSign string    `json:"internal_signature"`
	CustomerID   string    `json:"customer_id" validate:"required"`
	DeliverySvc  string    `json:"delivery_service" validate:"required"`
	ShardKey     string    `json:"shardkey" validate:"required"`
	SmID         uint      `json:"sm_id" validate:"required"`
	CreatedAt    time.Time `json:"date_created" validate:"required"`
	OOFShard     string    `json:"oof_shard" validate:"required"`
}

func (order Order) IsValid() validator.ValidationErrors {
	var (
		validate = validator.New()
		err      = validate.Struct(order)
		valErrs  validator.ValidationErrors
	)
	if errors.As(err, &valErrs) {
		return valErrs
	}
	return nil
}

func (order Order) GetJSON() []byte {
	orderJSON, _ := json.Marshal(order) // nolint:errcheck
	return orderJSON
}
