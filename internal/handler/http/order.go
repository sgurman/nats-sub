package httpapi

import (
	"net/http"

	"gitlab.com/sgurman/nats-sub/pkg/logger"

	"github.com/gorilla/mux"
)

const (
	orderTableHTML    = "order_table.html"
	orderNotFoundHTML = "order_not_found.html"
)

type orderHandler struct {
	uc orderUseCase
	l  logger.Interface
}

func initOrderRoute(r *mux.Router, uc orderUseCase, l logger.Interface) {
	h := orderHandler{uc, l}
	r.HandleFunc("/order", h.getByID).Methods(http.MethodGet)
	r.HandleFunc("/order/{id}", h.getByID).Methods(http.MethodGet)
}

func (h orderHandler) getByID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	ctx := r.Context()
	defer r.Body.Close()

	id := mux.Vars(r)["id"]
	if id == "" {
		id = r.FormValue("id")
		if id == "" {
			http.Redirect(w, r, "/", http.StatusMovedPermanently)
			return
		}
	}

	order, found, err := h.uc.ReadOrder(ctx, id)
	if err != nil {
		h.l.Errorf("httpapi - orderservice ReadOrder err: %s", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	} else if !found {
		orderTmplExec(w, orderNotFoundHTML, struct{ OrderID string }{id}, h.l)
		return
	}

	orderTmplExec(w, orderTableHTML, order, h.l)
}

func orderTmplExec(w http.ResponseWriter, tmplName string, data any, l logger.Interface) {
	if err := orderTmpl.ExecuteTemplate(w, tmplName, data); err != nil {
		l.Errorf("httpapi - %s template exec err: %s", tmplName, err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}
}
