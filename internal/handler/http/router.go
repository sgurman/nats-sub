package httpapi

import (
	"context"
	"fmt"
	"html/template"
	"net/http"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"
	"gitlab.com/sgurman/nats-sub/pkg/logger"

	"github.com/gorilla/mux"
)

const (
	indexPath = "./ui/static/html/index.html"
	tmplGlob  = "./ui/template/*.html"
)

var (
	orderTmpl = template.New("order")
)

type orderUseCase interface {
	ReadOrder(ctx context.Context, id string) (entity.Order, bool, error)
}

func NewRouter(uc orderUseCase, l logger.Interface) (http.Handler, error) {
	var err error
	if orderTmpl, err = orderTmpl.ParseGlob(tmplGlob); err != nil {
		return nil, fmt.Errorf("httpapi - order template ParseGlob err: %w", err)
	}

	r := mux.NewRouter()

	r.PathPrefix("/static/").Handler(http.StripPrefix(
		"/static/",
		http.FileServer(http.Dir("./ui/static")),
	))

	{
		s := r.PathPrefix("/api").Subrouter()
		initOrderRoute(s, uc, l)
	}

	r.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, indexPath)
	})

	return r, nil
}
