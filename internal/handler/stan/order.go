package stansub

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"
	"gitlab.com/sgurman/nats-sub/pkg/logger"

	"github.com/nats-io/stan.go"
)

const (
	orderSubject     = "orders"
	orderDurableName = "orders-durable"
)

type orderHandler struct {
	uc orderUseCase
	l  logger.Interface
}

func initOrderSub(sc stan.Conn, uc orderUseCase, l logger.Interface) error {
	h := orderHandler{uc, l}
	_, err := sc.Subscribe(
		orderSubject,
		h.orderSubscribe,
		stan.SetManualAckMode(),
		stan.DurableName(orderDurableName),
	)
	if err != nil {
		return fmt.Errorf("stansub - conn Subscribe err: %w", err)
	}
	return nil
}

func (h orderHandler) orderSubscribe(msg *stan.Msg) {
	defer ack(msg, h.l)

	var order entity.Order
	if err := json.Unmarshal(msg.Data, &order); err != nil {
		h.l.Errorf("stansub - msg Unmarshal err: %s", err)
		return
	}

	if errs := order.IsValid(); errs != nil {
		h.l.Errorf("stansub - order validation err: %s", errs)
		return
	}

	if err := h.uc.CreateOrder(context.Background(), order); err != nil {
		h.l.Errorf("stansub - orderservice CreateOrder err: %s", err)
		return
	}

	h.l.Infof("stansub - created order with uid `%s`", order.UID)
}

func ack(msg *stan.Msg, l logger.Interface) {
	if err := msg.Ack(); err != nil {
		l.Errorf("stansub - msg Ack err: %s", err)
	}
}
