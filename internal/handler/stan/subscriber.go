package stansub

import (
	"context"
	"fmt"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"
	"gitlab.com/sgurman/nats-sub/pkg/logger"

	"github.com/nats-io/stan.go"
)

type orderUseCase interface {
	CreateOrder(ctx context.Context, order entity.Order) error
}

func New(sc stan.Conn, uc orderUseCase, l logger.Interface) error {
	if err := initOrderSub(sc, uc, l); err != nil {
		return fmt.Errorf("stansub - initOrderSub err: %w", err)
	}
	return nil
}
