package localmemory

import (
	"errors"
	"sync"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"
)

var (
	ErrBadID = errors.New("localmemory err: empty order uid")
)

type OrderMemoryCache struct {
	cache map[string]entity.Order
	mu    sync.RWMutex
}

func NewOrderCache() *OrderMemoryCache {
	return &OrderMemoryCache{
		cache: make(map[string]entity.Order),
		mu:    sync.RWMutex{},
	}
}

func (mc *OrderMemoryCache) GetOne(id string) (entity.Order, bool, error) {
	if id == "" {
		return entity.Order{}, false, ErrBadID
	}
	mc.mu.RLock()
	order, found := mc.cache[id]
	mc.mu.RUnlock()
	return order, found, nil
}

func (mc *OrderMemoryCache) SetOne(order entity.Order) error {
	if order.UID == "" {
		return ErrBadID
	}
	mc.mu.Lock()
	mc.cache[order.UID] = order
	mc.mu.Unlock()
	return nil
}

func (mc *OrderMemoryCache) SetAll(orders []entity.Order) error {
	for _, order := range orders {
		if order.UID == "" {
			return ErrBadID
		}
	}
	mc.mu.Lock()
	for _, order := range orders {
		mc.cache[order.UID] = order
	}
	mc.mu.Unlock()
	return nil
}
