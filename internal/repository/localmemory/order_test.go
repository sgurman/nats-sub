package localmemory

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"

	"github.com/brianvoe/gofakeit/v6"
)

type testCase struct {
	id       string
	order    entity.Order
	orders   []entity.Order
	expected interface{}
	isFound  bool
	isErr    bool
	err      error
}

func getMockCache(size int, ids ...string) *OrderMemoryCache {
	mc := NewOrderCache()
	for i := 0; i < size; i++ {
		var order entity.Order
		gofakeit.Struct(&order)
		order.UID = ids[i]
		mc.cache[ids[i]] = order
	}
	return mc
}

func getNewOrder(id string) entity.Order {
	var order entity.Order
	gofakeit.Struct(&order)
	order.UID = id
	return order
}

func TestGetOne(t *testing.T) {
	mc := getMockCache(3, "id1", "id2", "id3")
	invalidID := ""
	existID := "id2"
	order2 := mc.cache[existID]

	cases := []testCase{
		{
			id:       invalidID,
			expected: entity.Order{},
			isFound:  false,
			isErr:    true,
			err:      ErrBadID,
		},
		{
			id:       existID,
			expected: order2,
			isFound:  true,
			isErr:    false,
			err:      nil,
		},
		{
			id:       "nonexistent id",
			expected: entity.Order{},
			isFound:  false,
			isErr:    false,
			err:      nil,
		},
	}

	expectedCache := map[string]entity.Order{
		"id1": mc.cache["id1"],
		"id2": order2,
		"id3": mc.cache["id3"],
	}

	for idx, item := range cases {
		result, found, err := mc.GetOne(item.id)

		if item.isErr && err == nil {
			t.Errorf("[%d] expected error, got nil", idx)
		}
		if item.isErr && err != nil && !errors.Is(item.err, err) {
			t.Errorf("[%d] error not match:\n\twant: %s\n\thave: %s", idx, item.err, err)
		}
		if !item.isErr && err != nil {
			t.Errorf("[%d] unexpected error: %s", idx, err)
		}
		if found != item.isFound {
			t.Errorf("[%d] found flag not match:\n\twant: %#v\n\thave: %#v", idx, item.isFound, found)
		}
		if !reflect.DeepEqual(item.expected, result) {
			t.Errorf("[%d] result not match:\n\twant: %#v\n\thave: %#v", idx, item.expected, result)
		}
	}

	if !reflect.DeepEqual(expectedCache, mc.cache) {
		t.Errorf("cache not match:\n\twant: %#v\n\thave: %#v", expectedCache, mc.cache)
	}
}

func TestSetOne(t *testing.T) {
	newOrder3 := getNewOrder("id3")
	newOrder4 := getNewOrder("id4")
	invalidOrder := getNewOrder("")

	cases := []testCase{
		{
			order: newOrder3,
			isErr: false,
			err:   nil,
		},
		{
			order: newOrder4,
			isErr: false,
			err:   nil,
		},
		{
			order: invalidOrder,
			isErr: true,
			err:   ErrBadID,
		},
	}

	mc := getMockCache(2, "id1", "id2")
	expectedCache := map[string]entity.Order{
		"id1": mc.cache["id1"],
		"id2": mc.cache["id2"],
		"id3": newOrder3,
		"id4": newOrder4,
	}

	for idx, item := range cases {
		err := mc.SetOne(item.order)

		if item.isErr && err == nil {
			t.Errorf("[%d] expected error, got nil", idx)
		}
		if item.isErr && err != nil && !errors.Is(item.err, err) {
			t.Errorf("[%d] error not match:\n\twant: %s\n\thave: %s", idx, item.err, err)
		}
		if !item.isErr && err != nil {
			t.Errorf("[%d] unexpected error: %s", idx, err)
		}
	}

	if !reflect.DeepEqual(expectedCache, mc.cache) {
		t.Errorf("cache not match:\n\twant: %#v\n\thave: %#v", expectedCache, mc.cache)
	}
}

func TestSetAll(t *testing.T) {
	newOrder3 := getNewOrder("id3")
	newOrder4 := getNewOrder("id4")
	newOrder5 := getNewOrder("id5")
	invalidOrder := getNewOrder("")

	cases := []testCase{
		{
			orders: []entity.Order{newOrder3, newOrder4},
			isErr:  false,
			err:    nil,
		},
		{
			orders: nil,
			isErr:  false,
			err:    nil,
		},
		{
			orders: []entity.Order{newOrder5, invalidOrder},
			isErr:  true,
			err:    ErrBadID,
		},
	}

	mc := getMockCache(2, "id1", "id2")
	expectedCache := map[string]entity.Order{
		"id1": mc.cache["id1"],
		"id2": mc.cache["id2"],
		"id3": newOrder3,
		"id4": newOrder4,
	}

	for idx, item := range cases {
		err := mc.SetAll(item.orders)

		if item.isErr && err == nil {
			t.Errorf("[%d] expected error, got nil", idx)
		}
		if item.isErr && err != nil && !errors.Is(item.err, err) {
			t.Errorf("[%d] error not match:\n\twant: %s\n\thave: %s", idx, item.err, err)
		}
		if !item.isErr && err != nil {
			t.Errorf("[%d] unexpected error: %s", idx, err)
		}
	}

	if !reflect.DeepEqual(expectedCache, mc.cache) {
		t.Errorf("cache not match: expected %#v, got %#v", expectedCache, mc.cache)
	}
}
