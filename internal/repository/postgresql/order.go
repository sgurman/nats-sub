package postgresql

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"
	"gitlab.com/sgurman/nats-sub/pkg/postgres"

	_ "github.com/jackc/pgx/v5/stdlib"
)

var (
	ErrBadID      = errors.New("psql err: empty order uid")
	ErrNRowInsert = errors.New("psql - order row insert err")
)

const (
	attrID      = "order_id"
	attrData    = "order_data"
	ordersTable = "orders"
)

type OrderRepository struct {
	postgres.Postgres
}

func NewOrderRepository(pg postgres.Postgres) OrderRepository {
	return OrderRepository{pg}
}

func (repo OrderRepository) GetAll(ctx context.Context) ([]entity.Order, error) {
	query := fmt.Sprintf("SELECT %s FROM %s", attrData, ordersTable)
	rows, err := repo.DB.QueryContext(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("psql - orders Query err: %w", err)
	}
	defer rows.Close()

	var orders []entity.Order
	for rows.Next() {
		var (
			orderJSON []byte
			order     entity.Order
		)
		if err = rows.Scan(&orderJSON); err != nil {
			return nil, fmt.Errorf("psql - order Scan err: %w", err)
		}
		if err = json.Unmarshal(orderJSON, &order); err != nil {
			return nil, fmt.Errorf("psql - order Unmarshal err: %w", err)
		}
		orders = append(orders, order)
	}

	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("psql - order rows err: %w", err)
	}

	return orders, nil
}

func (repo OrderRepository) InsertOne(ctx context.Context, order entity.Order) error {
	if order.UID == "" {
		return ErrBadID
	}

	query := fmt.Sprintf(
		"INSERT INTO %s (%s, %s) VALUES ($1, $2)",
		ordersTable, attrID, attrData,
	)
	res, err := repo.DB.ExecContext(ctx, query, order.UID, order.GetJSON())
	if err != nil {
		return fmt.Errorf("psql - order insert Exec err: %w", err)
	}

	nRows, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("psql - result RowsAffected err: %w", err)
	} else if nRows != 1 {
		return ErrNRowInsert
	}

	return nil
}
