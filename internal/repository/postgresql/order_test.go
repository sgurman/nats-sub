package postgresql

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"
	"gitlab.com/sgurman/nats-sub/pkg/postgres"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/jmoiron/sqlx"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func getNewOrder(id string) entity.Order {
	var order entity.Order
	gofakeit.Struct(&order)
	order.UID = id
	return order
}

func TestGetAll_OK(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock New err: %s", err)
	}
	dbx := sqlx.NewDb(db, "sqlmock")
	defer db.Close()

	query := fmt.Sprintf("SELECT %s FROM %s", attrData, ordersTable)
	expected := []entity.Order{getNewOrder("id1"), getNewOrder("id2")}

	rows := sqlmock.
		NewRows([]string{attrData}).
		AddRow(expected[0].GetJSON()).
		AddRow(expected[1].GetJSON())
	mock.
		ExpectQuery(query).
		WillReturnRows(rows)

	repo := NewOrderRepository(postgres.Postgres{DB: dbx})
	result, err := repo.GetAll(context.Background())

	if err != nil {
		t.Errorf("unexpected err: %s", err)
	}
	if !reflect.DeepEqual(expected, result) {
		t.Errorf("result not match:\n\twant: %#v\n\thave: %#v", expected, result)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetAll_QueryError(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock New err: %s", err)
	}
	dbx := sqlx.NewDb(db, "sqlmock")
	defer db.Close()

	query := fmt.Sprintf("SELECT %s FROM %s", attrData, ordersTable)
	expected := errors.New("query error")

	mock.
		ExpectQuery(query).
		WillReturnError(expected)

	repo := NewOrderRepository(postgres.Postgres{DB: dbx})
	result, err := repo.GetAll(context.Background())

	if result != nil {
		t.Errorf("unexpected result: %v", result)
	}
	if err == nil {
		t.Errorf("expected error, got nil")
	}
	if !errors.Is(err, expected) {
		t.Errorf("error not match:\n\twant: %s\n\thave: %s", expected, err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetAll_ScanError(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock New err: %s", err)
	}
	dbx := sqlx.NewDb(db, "sqlmock")
	defer db.Close()

	query := fmt.Sprintf("SELECT %s FROM %s", attrData, ordersTable)
	expected := "psql - order Scan err"

	rows := sqlmock.
		NewRows([]string{attrID, attrData}).
		AddRow("should not get id", sqlmock.AnyArg())
	mock.
		ExpectQuery(query).
		WillReturnRows(rows)

	repo := NewOrderRepository(postgres.Postgres{DB: dbx})
	result, err := repo.GetAll(context.Background())

	if result != nil {
		t.Errorf("unexpected result: %v", result)
	}
	if err == nil {
		t.Errorf("expected error, got nil")
	}
	if !strings.HasPrefix(err.Error(), expected) {
		t.Errorf("error not match:\n\twant: %s\n\thave: %s", expected, err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetAll_DecodeError(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock New err: %s", err)
	}
	dbx := sqlx.NewDb(db, "sqlmock")
	defer db.Close()

	query := fmt.Sprintf("SELECT %s FROM %s", attrData, ordersTable)
	expected := "psql - order Unmarshal err"

	rows := sqlmock.
		NewRows([]string{attrData}).
		AddRow("invalid json")
	mock.
		ExpectQuery(query).
		WillReturnRows(rows)

	repo := NewOrderRepository(postgres.Postgres{DB: dbx})
	result, err := repo.GetAll(context.Background())

	if result != nil {
		t.Errorf("unexpected result: %v", result)
	}
	if err == nil {
		t.Errorf("expected error, got nil")
	}
	if !strings.HasPrefix(err.Error(), expected) {
		t.Errorf("error not match:\n\twant: %s\n\thave: %s", expected, err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestGetAll_RowsError(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock New err: %s", err)
	}
	dbx := sqlx.NewDb(db, "sqlmock")
	defer db.Close()

	query := fmt.Sprintf("SELECT %s FROM %s", attrData, ordersTable)
	expected := errors.New("rows error")

	rows := sqlmock.
		NewRows([]string{attrData}).
		AddRow(nil).
		RowError(0, expected)
	mock.
		ExpectQuery(query).
		WillReturnRows(rows)

	repo := NewOrderRepository(postgres.Postgres{DB: dbx})
	result, err := repo.GetAll(context.Background())

	if result != nil {
		t.Errorf("unexpected result: %v", result)
	}
	if err == nil {
		t.Errorf("expected error, got nil")
	}
	if !errors.Is(err, expected) {
		t.Errorf("error not match:\n\twant: %s\n\thave: %s", expected, err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestInsertOne_OK(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock New err: %s", err)
	}
	dbx := sqlx.NewDb(db, "sqlmock")
	defer db.Close()

	query := fmt.Sprintf("INSERT INTO %s", ordersTable)
	order := getNewOrder("id")

	mock.
		ExpectExec(query).
		WithArgs(order.UID, order.GetJSON()).
		WillReturnResult(sqlmock.NewResult(1, 1))

	repo := NewOrderRepository(postgres.Postgres{DB: dbx})
	err = repo.InsertOne(context.Background(), order)

	if err != nil {
		t.Errorf("unexpected err: %s", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestInsertOne_BadOrderID(t *testing.T) {
	invalidOrder := getNewOrder("")
	expected := ErrBadID

	repo := NewOrderRepository(postgres.Postgres{DB: nil})
	err := repo.InsertOne(context.Background(), invalidOrder)

	if err == nil {
		t.Errorf("expected error, got nil")
	}
	if !errors.Is(err, expected) {
		t.Errorf("error not match:\n\twant: %s\n\thave: %s", expected, err)
	}
}

func TestInsertOne_ExecError(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock New err: %s", err)
	}
	dbx := sqlx.NewDb(db, "sqlmock")
	defer db.Close()

	query := fmt.Sprintf("INSERT INTO %s", ordersTable)
	order := getNewOrder("id")
	expected := errors.New("exec error")

	mock.
		ExpectExec(query).
		WithArgs(order.UID, order.GetJSON()).
		WillReturnError(expected)

	repo := NewOrderRepository(postgres.Postgres{DB: dbx})
	err = repo.InsertOne(context.Background(), order)

	if err == nil {
		t.Errorf("expected error, got nil")
	}
	if !errors.Is(err, expected) {
		t.Errorf("error not match:\n\twant: %s\n\thave: %s", expected, err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestInsertOne_RowsAffectedError(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock New err: %s", err)
	}
	dbx := sqlx.NewDb(db, "sqlmock")
	defer db.Close()

	query := fmt.Sprintf("INSERT INTO %s", ordersTable)
	order := getNewOrder("id")
	expected := errors.New("rows affected error")

	mock.
		ExpectExec(query).
		WithArgs(order.UID, order.GetJSON()).
		WillReturnResult(sqlmock.NewErrorResult(expected))

	repo := NewOrderRepository(postgres.Postgres{DB: dbx})
	err = repo.InsertOne(context.Background(), order)

	if err == nil {
		t.Errorf("expected error, got nil")
	}
	if !errors.Is(err, expected) {
		t.Errorf("error not match:\n\twant: %s\n\thave: %s", expected, err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestInsertOne_NRowsAffectedError(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock New err: %s", err)
	}
	dbx := sqlx.NewDb(db, "sqlmock")
	defer db.Close()

	query := fmt.Sprintf("INSERT INTO %s", ordersTable)
	order := getNewOrder("id")
	expected := ErrNRowInsert

	mock.
		ExpectExec(query).
		WithArgs(order.UID, order.GetJSON()).
		WillReturnResult(sqlmock.NewResult(0, 0))

	repo := NewOrderRepository(postgres.Postgres{DB: dbx})
	err = repo.InsertOne(context.Background(), order)

	if err == nil {
		t.Errorf("expected error, got nil")
	}
	if !errors.Is(err, expected) {
		t.Errorf("error not match:\n\twant: %s\n\thave: %s", expected, err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
