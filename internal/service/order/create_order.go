package orderservice

import (
	"context"
	"fmt"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"
)

func (svc Service) CreateOrder(ctx context.Context, order entity.Order) error {
	if err := svc.repo.InsertOne(ctx, order); err != nil {
		return fmt.Errorf("orderservice - repo InsertOne err: %w", err)
	}
	if err := svc.cache.SetOne(order); err != nil {
		return fmt.Errorf("orderservice - cache SetOne err: %w", err)
	}
	return nil
}
