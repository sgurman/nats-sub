package orderservice

import (
	"context"
	"fmt"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"
)

func (svc Service) ReadOrder(_ context.Context, id string) (entity.Order, bool, error) {
	order, found, err := svc.cache.GetOne(id)
	if err != nil {
		return entity.Order{}, false, fmt.Errorf("orderservice - cache Get err: %w", err)
	}
	return order, found, nil
}
