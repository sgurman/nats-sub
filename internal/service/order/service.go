package orderservice

import (
	"context"
	"fmt"

	"gitlab.com/sgurman/nats-sub/internal/domain/entity"
)

type orderStorage interface {
	GetAll(ctx context.Context) ([]entity.Order, error)
	InsertOne(ctx context.Context, order entity.Order) error
}

type orderCache interface {
	GetOne(id string) (entity.Order, bool, error)
	SetOne(order entity.Order) error
	SetAll(orders []entity.Order) error
}

type Service struct {
	repo  orderStorage
	cache orderCache
}

func New(os orderStorage, oc orderCache) (Service, error) {
	svc := Service{repo: os, cache: oc}
	if err := initCache(svc); err != nil {
		return Service{}, fmt.Errorf("orderservice - init Cache err: %w", err)
	}
	return svc, nil
}

func initCache(svc Service) error {
	orders, err := svc.repo.GetAll(context.Background())
	if err != nil {
		return fmt.Errorf("orderservice - storage GetAll err: %w", err)
	}
	if err = svc.cache.SetAll(orders); err != nil {
		return fmt.Errorf("orderservice - cache SetAll err: %w", err)
	}
	return nil
}
