package server

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

const (
	defaultIdleTimeout     = 60 * time.Second
	defaultShutdownTimeout = 3 * time.Second
)

type HTTPServer struct {
	server *http.Server
}

func New(port string, rwTimeout time.Duration, handler http.Handler) HTTPServer {
	return HTTPServer{&http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      handler,
		ReadTimeout:  rwTimeout,
		WriteTimeout: rwTimeout,
		IdleTimeout:  defaultIdleTimeout,
	},
	}
}

func (s HTTPServer) Start() error {
	return s.server.ListenAndServe()
}

func (s HTTPServer) Stop() error {
	ctx, cancel := context.WithTimeout(context.Background(), defaultShutdownTimeout)
	defer cancel()
	return s.server.Shutdown(ctx)
}
