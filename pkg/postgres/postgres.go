package postgres

import (
	"fmt"

	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
)

type Postgres struct {
	DB *sqlx.DB
}

func New(addr, dbname, user, passw string) (Postgres, error) {
	pgURL := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		user, passw, addr, dbname,
	)
	db, err := sqlx.Open("pgx", pgURL)
	if err != nil {
		return Postgres{}, fmt.Errorf("pg - Open err: %w", err)
	}
	if err = db.Ping(); err != nil {
		return Postgres{}, fmt.Errorf("pg - Ping err: %w", err)
	}
	return Postgres{DB: db}, nil
}

func (pg Postgres) Close() error {
	if pg.DB == nil {
		return fmt.Errorf("pg err: nil db pointer")
	}
	return pg.DB.Close()
}
